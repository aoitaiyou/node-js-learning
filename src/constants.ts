import * as dotenv from "dotenv";

dotenv.config();

if (!process.env.PORT) {
  // validate undefined
  process.exit(1);
}

export const PORT: number = parseInt(process.env.Port!);
export const PostgresPort: number = parseInt(process.env.POSTGRES_PORT!);
export const PostgresHost: string = process.env.POSTGRES_HOST!;
export const PostgresDatabase: string = process.env.POSTGRES_DATABASE!;
export const PostgresUser: string = process.env.POSTGRES_USER!;
export const PostgresPassword: string = process.env.POSTGRES_PASSWORD!;
