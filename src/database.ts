import { Pool, Client } from "pg";
import * as ct from "./constants";

export const pool: Pool = new Pool({
  port: ct.PostgresPort,
  host: ct.PostgresHost,
  user: ct.PostgresUser,
  password: ct.PostgresPassword,
  database: ct.PostgresDatabase,
});
