import express from "express";
import { PORT, PostgresPort } from "./constants";
import { pool } from "./database";
import { Query, QueryResult } from "pg";

console.clear();
console.log("===================================");

const app = express();
app.use(express.json());

app.get("/", function (req, res) {
  res.send("Hello World");
});

var server = app.listen(PORT, function () {
  // var host = server.address().address;
  var port = PORT;

  console.log(
    "Ung dung Node.js dang lang nghe tai dia chi: http://localhost:%s",
    // host,
    port
  );
});

const database = pool;

database.connect(function (err: any) {
  if (err) throw err;
  console.log("Connected!");
});

//! STUDENTS
/// get all student if code == null
app.route("/students/get").post((req, res) => {
  try {
    var requestData = req.body;
    var query;
    var queryParam: unknown[] = [];

    if (requestData.code) {
      query = `SELECT * From student WHERE code = $1`;
      queryParam.push(requestData.code);
    } else query = `SELECT * From student`;

    database.query(query, queryParam, (err: any, response: any) => {
      if (err) {
        return res.json({
          status: false,
          message: "Có lỗi xảy ra",
        });
      }
      if (response.rowCount == 0) {
        return res.json({
          status: true,
          message: "Mã không tồn tại",
        });
      }
      return res.json({
        status: true,
        data: response.rows,
      });
    });
  } catch (error) {
    return res.json({
      status: false,
      message: error,
    });
  }
});

/// add student
app.route("/students/add").post((req, res) => {
  try {
    var requestData = req.body;

    if (!requestData.name || !requestData.code || !requestData.dob) {
      throw "Xin điền đầy đủ thông tin.";
    }

    const query = `INSERT INTO public.student (name, code, dob) VALUES($1, $2, $3)`;
    const queryParam: any[] = [
      requestData.name,
      requestData.code,
      requestData.dob,
    ];

    database.query(query, queryParam, (err: any, response: any) => {
      if (err) {
        if (err.code == "23505")
          return res.json({
            status: false,
            message: "Đã tồn tại",
          });
        return res.json({
          status: false,
          message: "Có lỗi xảy ra",
        });
      } else {
        return res.json({
          status: true,
          message: "Đã thêm thành công.",
        });
      }
    });
  } catch (error) {
    return res.json({
      status: false,
      message: error,
    });
  }
});

/// update student
app.route("/students/update").post(async (req, res) => {
  try {
    var requestData = req.body;

    if (!requestData.code) {
      throw "Xin điền mã học sinh.";
    } else if (!requestData.newCode || !requestData.name || !requestData.dob) {
      throw "Xin điền đầy đủ thông tin cần thay đổi.";
    }
    const selectQuery = `SELECT * FROM public.student WHERE code = $1`;
    var selectParams = [requestData.code];
    var result: QueryResult = await database.query(selectQuery, selectParams);
    if (result.rowCount === 0) {
      return res.json({
        status: true,
        message: "Mã không tồn tại.",
      });
    }
    var queryParam = [
      requestData.newCode,
      requestData.dob,
      requestData.name,
      requestData.code,
    ];
    var query = ` UPDATE public.student SET code=$1, dob=$2, name=$3 WHERE code = $4 `;
    database.query(query, queryParam, (err: any, response: any) => {
      if (err)
        return res.json({
          status: false,
          message: "Có lỗi xảy ra",
        });
      else {
        return res.json({
          status: true,
          message: "Thay đổi thành công.",
        });
      }
    });
  } catch (error) {
    return res.json({
      status: false,
      message: error,
    });
  }
});

/// delete student
app.route("/students/delete").post(async (req, res) => {
  try {
    var requestData = req.body;

    if (!requestData.code) {
      throw "Xin điền mã học sinh.";
    }
    var queryParam = [requestData.code];

    const result = await database.query(
      `SELECT * From student WHERE code =$1`,
      queryParam
    );
    if (result.rowCount == 0) {
      return res.json({
        status: true,
        message: "Mã không tồn tại.",
      });
    }
    const query = ` DELETE FROM public.student WHERE code = $1`;

    database.query(query, queryParam, (err: any, response: any) => {
      if (err) throw err;
      else {
        return res.json({
          status: true,
          message: "Xóa thành công.",
        });
      }
    });
  } catch (error) {
    return res.json({
      status: false,
      message: error,
    });
  }
});

//! TEACHER
/// get all teacher if code == null
app.route("/teacher/get").post((req, res) => {
  try {
    var requestData = req.body;
    var query = `
    SELECT 
      t.name, 
      t.code, 
      t.workstartdate,
      s.name as "subjectName", 
      s.code as "subjectCode"

    From teacher t

    LEFT JOIN subject s 

    ON t.subject_id = s.id

    `;
    var queryParam: unknown[] = [];

    if (requestData.code) {
      query += `WHERE t.code = $1`;
      queryParam.push(requestData.code);
    }

    database.query(query, queryParam, (err: any, response: any) => {
      if (err) {
        return res.json({
          status: true,
          data: [],
        });
      }
      if (response.rowCount == 0) {
        return res.json({
          status: true,
          data: [],
        });
      }
      return res.json({
        status: true,
        data: response.rows,
      });
    });
  } catch (error) {
    return res.json({
      status: false,
      message: error,
    });
  }
});

/// add teacher
app.route("/teacher/add").post(async (req, res) => {
  try {
    var requestData = req.body;

    if (
      !requestData.name ||
      !requestData.code ||
      !requestData.workstartdate ||
      !requestData.subjectCode
    ) {
      throw "Xin điền đầy đủ thông tin.";
    }

    var subjectResult: QueryResult = await database.query(
      `SELECT subject.id FROM subject WHERE code = $1`,
      [requestData.subjectCode]
    );

    if (subjectResult.rowCount == 0) {
      return res.json({
        status: true,
        message: "Không tồn tại môn học",
      });
    }
    const subjectCode = subjectResult.rows[0].id;
    const query = `INSERT INTO public.teacher (name, code, workstartdate, subject_id) VALUES($1, $2, $3, $4)`;
    const queryParam: any[] = [
      requestData.name,
      requestData.code,
      requestData.workstartdate,
      subjectCode,
    ];

    database.query(query, queryParam, (err: any, response: any) => {
      if (err) {
        if (err.code == "23505")
          return res.json({
            status: false,
            message: "Đã tồn tại",
          });
        return res.json({
          status: false,
          message: "Có lỗi xảy ra",
        });
      } else {
        return res.json({
          status: true,
          message: "Đã thêm thành công.",
        });
      }
    });
  } catch (error) {
    return res.json({
      status: false,
      message: error,
    });
  }
});

/// update teacher
app.route("/teacher/update").post(async (req, res) => {
  try {
    var requestData = req.body;

    if (
      !requestData.code ||
      !requestData.newCode ||
      !requestData.name ||
      !requestData.workstartdate ||
      !requestData.subjectCode
    ) {
      throw "Xin điền đầy đủ thông tin cần thay đổi.";
    }
    const selectQuery = `SELECT * FROM public.teacher WHERE code = $1`;
    var selectParams = [requestData.code];
    var result: QueryResult = await database.query(selectQuery, selectParams);
    if (result.rowCount === 0) {
      return res.json({
        status: true,
        message: "Mã không tồn tại.",
      });
    }
    var queryParam = [
      requestData.newCode,
      requestData.workstartdate,
      requestData.name,
      requestData.subjectCode,
      requestData.code,
    ];

    var query = ` 
    UPDATE public.teacher 

    SET 
    code=$1, workstartdate=$2, 
    name=$3, subject_id=
      (
        SELECT id
        FROM subject
        WHERE subject.code = $4
      )
      
    WHERE code = $5
    `;

    database.query(query, queryParam, (err: any, response: any) => {
      if (err)
        return res.json({
          status: false,
          message: "Có lỗi xảy ra",
        });
      else {
        return res.json({
          status: true,
          message: "Thay đổi thành công.",
        });
      }
    });
  } catch (error) {
    return res.json({
      status: false,
      message: error,
    });
  }
});
/// delete teacher
app.route("/teacher/delete").post((req, res) => {
  try {
    var requestData = req.body;

    if (!requestData.code) {
      throw "Xin điền mã giáo viên.";
    }

    var query = ` DELETE FROM public.teacher WHERE code = '${requestData.code}'`;

    database.query(query, (err: any, response: any) => {
      if (err) throw err;
      else {
        return res.json({
          status: true,
          message: "Xóa thành công.",
        });
      }
    });
  } catch (error) {
    return res.json({
      status: false,
      message: error,
    });
  }
});

//! Test
/// get all, get code
app.route("/test/get").post((req, res) => {
  try {
    const requestData = req.body;
    var query = `
            SELECT 
                test.code, 
                teacher.code as teacher_code, 
                teacher.name as teacher_name,
                subject.name AS subject_name,
                subject.code AS subject_code
            FROM test 

            LEFT JOIN teacher 
            ON test.teacher_id = teacher.id

            LEFT JOIN subject 
            ON teacher.subject_id = subject.id

            `;
    var queryParam = [];

    if (requestData.code) {
      query += `WHERE test.code = $1`;
      queryParam.push(requestData.code);
    }

    database.query(query, queryParam, (err: Error, result: QueryResult) => {
      if (err) {
        return res.json({
          status: false,
          message: "Đã có lỗi xảy ra",
        });
      }
      return res.json({
        status: true,
        data: result.rows,
      });
    });
  } catch (error) {
    return res.json({
      status: false,
      message: "Đã có lỗi xảy ra",
    });
  }
});
/// add test
app.route("/test/add").post((req, res) => {
  try {
    const requestData = req.body;
    if (!requestData.code || !requestData.teacherCode) {
      return res.json({
        status: false,
        message: "Xin vui lòng điền đầy đủ thông tin",
      });
    }

    var query = `
        INSERT INTO public.test
        (
            teacher_id, 
            code
        )
        VALUES(
            (
                SELECT id 
                FROM teacher 
                WHERE code = $1
            ), 
        $2
        );`;
    var queryParam = [requestData.teacherCode, requestData.code];

    database.query(query, queryParam, (err: Error, result: QueryResult) => {
      if (err) {
        return res.json({
          status: false,
          message: "Đã có lỗi xảy ra",
        });
      }
      return res.json({
        status: true,
        message: "Thêm thành công",
      });
    });
  } catch (error) {
    return res.json({
      status: false,
      message: "Đã có lỗi xảy ra",
    });
  }
});
/// update test
app.route("/test/update").post((req, res) => {
  try {
    const requestData = req.body;
    if (!requestData.code || !requestData.teacherCode || !requestData.newCode) {
      return res.json({
        status: false,
        message: "Xin vui lòng điền đầy đủ thông tin",
      });
    }

    var query = `UPDATE test
        SET code = $1, teacher_id = (
            SELECT teacher_id
            FROM teacher
            WHERE teacher.code = $2
        )
        WHERE test.code = $3;`;
    var queryParam = [
      requestData.newCode,
      requestData.teacherCode,
      requestData.code,
    ];

    database.query(query, queryParam, (err: Error, result: QueryResult) => {
      if (err) {
        return res.json({
          status: false,
          message: "Đã có lỗi xảy ra",
        });
      }
      return res.json({
        status: true,
        message: "Cập nhật thành công",
      });
    });
  } catch (error) {
    return res.json({
      status: false,
      message: "Đã có lỗi xảy ra",
    });
  }
});
/// delete test
app.route("/test/delete").post(async (req, res) => {
  try {
    const requestData = req.body;
    if (!requestData.code) {
      return res.json({
        status: false,
        message: "Xin vui lòng điền đầy đủ thông tin",
      });
    }

    var result = await database.query(`SELECT * FROM test WHERE code = $1`, [
      requestData.code,
    ]);
    var query = `DELETE FROM public.test
        WHERE code = $1;
        `;
    var queryParam = [requestData.code];

    database.query(query, queryParam, (err: Error, result: QueryResult) => {
      if (err) {
        return res.json({
          status: false,
          message: "Đã có lỗi xảy ra",
        });
      }
      return res.json({
        status: true,
        message: "Xóa thành công",
      });
    });
  } catch (error) {
    return res.json({
      status: false,
      message: "Đã có lỗi xảy ra",
    });
  }
});
/// get all students with test code
app.route("/test/get-student").post(async (req, res) => {
  try {
    const requestData = req.body;
    if (!requestData.code) {
      return res.json({
        status: false,
        message: "Vui lòng điền mã bài kiểm tra",
      });
    }
    var query = `
        SELECT student.code, student.name, teacher.name, teacher.code
        FROM student
        WHERE id IN (
            SELECT student_id
            FROM student_test
            WHERE test_id in (
                SELECT id
                FROM test
                WHERE test.code = $1
            )
        )
        `;
    var queryParam = [];
    if (requestData.code) {
      queryParam.push(requestData.code);
    } else {
      query = `SELECT id FROM test`;
    }

    var result = await database.query(query, queryParam);

    return res.json({
      status: true,
      data: result.rows,
    });
  } catch (error) {
    return res.json({
      status: false,
      message: "Đã có lỗi xảy ra",
    });
  }
});
/// get all teacher with 0 test code
app.route("/test/get-teacher").post(async (req, res) => {
  try {
    var query = `
        SELECT teacher.name, teacher.code, subject.name as subject_name
        FROM teacher 
        JOIN subject  ON teacher.subject_id = subject.id
        WHERE NOT EXISTS (
            SELECT *
            FROM test
            WHERE teacher.id = test.teacher_id
        );
        `;
    var result = await database.query(query);
    return res.json({
      status: true,
      data: result.rows,
    });
  } catch (error) {
    return res.json({
      status: false,
      message: "Đã có lỗi xảy ra",
    });
  }
});

//! Subject
/// get all, get code
app.route("/subject/get").post((req, res) => {
  try {
    const requestData = req.body;
    var query = `
            SELECT 
                subject.code,
                subject.name,
                teacher.code as teacher_code, 
                teacher.name as teacher_name
            FROM subject 
            LEFT JOIN teacher  
            ON teacher.subject_id = subject.id

        `;
    var queryParam = [];
    if (requestData.code) {
      query += `WHERE subject.code = $1`;
      queryParam.push(requestData.code);
    }

    database.query(query, queryParam, (err: Error, result: QueryResult) => {
      if (err) {
        return res.json({
          status: false,
          message: "Đã có lỗi xảy ra",
        });
      }
      return res.json({
        status: true,
        data: result.rows,
      });
    });
  } catch (error) {
    return res.json({
      status: false,
      message: "Đã có lỗi xảy ra",
    });
  }
});
/// add subject
app.route("/subject/add").post((req, res) => {
  try {
    const requestData = req.body;
    if (!requestData.code || !requestData.name) {
      return res.json({
        status: false,
        message: "Xin vui lòng điền đầy đủ thông tin",
      });
    }

    var query = `
        INSERT INTO public.subject
        (
            name, code
        )
        VALUES(
            $1, $2
        );`;
    var queryParam = [requestData.name, requestData.code];

    database.query(query, queryParam, (err: Error, result: QueryResult) => {
      if (err) {
        return res.json({
          status: false,
          message: "Đã có lỗi xảy ra",
        });
      }
      return res.json({
        status: true,
        message: "Thêm thành công",
      });
    });
  } catch (error) {
    return res.json({
      status: false,
      message: "Đã có lỗi xảy ra",
    });
  }
});
/// update subject
app.route("/subject/update").post((req, res) => {
  try {
    const requestData = req.body;
    if (!requestData.code || !requestData.name || !requestData.newCode) {
      return res.json({
        status: false,
        message: "Xin vui lòng điền đầy đủ thông tin",
      });
    }

    var query = `
        UPDATE subject
        SET code = $1, name = $2
        WHERE subject.code = $3;`;
    var queryParam = [requestData.newCode, requestData.name, requestData.code];

    database.query(query, queryParam, (err: Error, result: QueryResult) => {
      if (err) {
        return res.json({
          status: false,
          message: "Đã có lỗi xảy ra",
        });
      }
      return res.json({
        status: true,
        message: "Cập nhật thành công",
      });
    });
  } catch (error) {
    return res.json({
      status: false,
      message: "Đã có lỗi xảy ra",
    });
  }
});
/// delete subject
app.route("/subject/delete").post(async (req, res) => {
  try {
    const requestData = req.body;
    if (!requestData.code) {
      return res.json({
        status: false,
        message: "Xin vui lòng điền đầy đủ thông tin",
      });
    }

    var result = await database.query(`SELECT * FROM subject WHERE code = $1`, [
      requestData.code,
    ]);
    var query = `
        DELETE FROM public.subject
        WHERE code = $1;
        `;
    var queryParam = [requestData.code];

    database.query(query, queryParam, (err: Error, result: QueryResult) => {
      if (err) {
        return res.json({
          status: false,
          message: "Đã có lỗi xảy ra",
        });
      }
      return res.json({
        status: true,
        message: "Xóa thành công",
      });
    });
  } catch (error) {
    return res.json({
      status: false,
      message: "Đã có lỗi xảy ra",
    });
  }
});

//! Student - Subject
/// get using studentCode, testCode
app.route("/stu-sub/get").post((req, res) => {
  try {
    const requestData = req.body;
    var query = `
    SELECT 
      s.id AS student_id,
      s.name AS student_name,
      s.code AS student_code,
      ss.subject_id,
      sub.code AS sub_code,
      sub.name AS sub_name

    FROM student_subject ss

    JOIN student s ON s.id = ss.student_id
    
    JOIN subject sub ON ss.subject_id =sub.id

    `;
    var queryParam = [];
    if (requestData.code) {
      query += `WHERE s.code = $1`;
      queryParam.push(requestData.code);
    }

    database.query(query, queryParam, (err: Error, result: QueryResult) => {
      if (err) {
        return res.json({
          status: false,
          message: "Đã có lỗi xảy ra",
        });
      }
      return res.json({
        status: true,
        data: result.rows,
      });
    });
  } catch (error) {
    return res.json({
      status: false,
      message: "Đã có lỗi xảy ra",
    });
  }
});
/// add subject
app.route("/stu-sub/add").post((req, res) => {
  try {
    const requestData = req.body;
    if (!requestData.studentCode || !requestData.subjectCode) {
      return res.json({
        status: false,
        message: "Xin vui lòng điền đầy đủ thông tin",
      });
    }

    var query = `
        INSERT INTO public.student_subject
        (
            student_id, subject_id
        )
        VALUES(
          (
            SELECT student.id
            FROM student
            WHERE student.code = $1
          ),
          (
            SELECT subject.id
            FROM subject
            WHERE subject.code = $2
          )
        );`;
    var queryParam = [requestData.studentCode, requestData.subjectCode];

    database.query(query, queryParam, (err: any, result: QueryResult) => {
      if (err) {
        if (err.code == "23505") {
          return res.json({
            status: false,
            message: "Đã tồn tại",
          });
        }
        return res.json({
          status: false,
          message: "Đã có lỗi xảy ra",
        });
      }
      return res.json({
        status: true,
        message: "Thêm thành công",
      });
    });
  } catch (error) {
    return res.json({
      status: false,
      message: "Đã có lỗi xảy ra",
    });
  }
});
/// update subject
app.route("/stu-sub/update").post(async (req, res) => {
  try {
    const requestData = req.body;
    if (
      !requestData.studentCode ||
      !requestData.oldSubjectCode ||
      !requestData.newSubjectCode
    ) {
      return res.json({
        status: false,
        message: "Xin vui lòng điền đầy đủ thông tin",
      });
    }
    var queryParam = [requestData.studentCode, requestData.oldSubjectCode];
    var query = `
    SELECT *
    FROM student_subject
    WHERE
    (
      student_id =
      (
        SELECT student.id
        FROM student
        WHERE student.code = $1
      )
      AND
      subject_id =
      (
        SELECT subject.id
        FROM subject
        WHERE subject.code = $2
      )
    )
    `;
    var result: QueryResult = await database.query(query, queryParam);
    if (result.rowCount === 0) {
      return res.json({
        status: true,
        message: "Mã không tồn tại.",
      });
    }
    queryParam.push(requestData.newSubjectCode);
    var query = `
        UPDATE student_subject
        SET  subject_id =
        (
          SELECT subject.id
          FROM subject
          WHERE subject.code = $3
        )
        WHERE 
        (
          student_id =
          (
            SELECT student.id
            FROM student
            WHERE student.code = $1
          ) 

          AND 

          subject_id =
          (
            SELECT subject.id
            FROM subject
            WHERE subject.code = $2
          )
        )
        `;

    database.query(query, queryParam, (err: Error, result: QueryResult) => {
      if (err) {
        return res.json({
          status: false,
          message: "Đã có lỗi xảy ra",
        });
      }
      return res.json({
        status: true,
        message: "Cập nhật thành công",
      });
    });
  } catch (error) {
    return res.json({
      status: false,
      message: "Đã có lỗi xảy ra",
    });
  }
});
/// delete subject
app.route("/stu-sub/delete").post(async (req, res) => {
  try {
    const requestData = req.body;
    if (!requestData.studentCode || !requestData.subjectCode) {
      return res.json({
        status: false,
        message: "Xin vui lòng điền đầy đủ thông tin",
      });
    }
    var queryParam = [requestData.studentCode, requestData.subjectCode];

    var result = await database.query(
      `
    SELECT * 
    FROM student_subject 
    WHERE
    (
      student_id =
      (
        SELECT student.id
        FROM student
        WHERE student.code = $1
      )
      AND
      subject_id =
      (
        SELECT subject.id
        FROM subject
        WHERE subject.code = $2
      )
    )
    `,
      queryParam
    );

    if (result.rowCount == 0) {
      return res.json({
        status: false,
        message: "Không tồn tại học sinh trong lớp này.",
      });
    }

    var query = `
        DELETE FROM public.student_subject
        WHERE
        (
          student_id =
          (
            SELECT student.id
            FROM student
            WHERE student.code = $1
          )
          AND
          subject_id =
          (
            SELECT subject.id
            FROM subject
            WHERE subject.code = $2
          )
        )
        `;

    database.query(query, queryParam, (err: Error, result: QueryResult) => {
      if (err) {
        return res.json({
          status: false,
          message: "Đã có lỗi xảy ra",
        });
      }
      return res.json({
        status: true,
        message: "Xóa thành công",
      });
    });
  } catch (error) {
    return res.json({
      status: false,
      message: "Đã có lỗi xảy ra",
    });
  }
});

//! Student - Test
/// get using studentCode, testCode
app.route("/stu-test/get").post((req, res) => {
  try {
    const requestData = req.body;
    var query = `
    SELECT 
      s.name AS student_name,
      s.code AS student_code,
      test.code AS test_code,
      st.grade AS grade

    FROM student_test st

    JOIN student s ON s.id = st.student_id
    
    JOIN test  ON st.test_id =test.id

    `;
    var queryParam = [];
    if (requestData.code) {
      query += `WHERE s.code = $1`;
      queryParam.push(requestData.code);
    }

    database.query(query, queryParam, (err: Error, result: QueryResult) => {
      if (err) {
        return res.json({
          status: false,
          message: "Đã có lỗi xảy ra",
        });
      }
      return res.json({
        status: true,
        data: result.rows,
      });
    });
  } catch (error) {
    return res.json({
      status: false,
      message: "Đã có lỗi xảy ra",
    });
  }
});
/// add subject
app.route("/stu-test/add").post((req, res) => {
  try {
    const requestData = req.body;
    if (!requestData.studentCode || !requestData.testCode) {
      return res.json({
        status: false,
        message: "Xin vui lòng điền đầy đủ thông tin",
      });
    }

    var query = `
        INSERT INTO public.student_test
        (
            student_id, test_id, grade
        )
        VALUES(
          (
            SELECT student.id
            FROM student
            WHERE student.code = $1
          ),
          (
            SELECT test.id
            FROM test
            WHERE test.code = $2
          ),
          $3
        );`;
    var queryParam = [
      requestData.studentCode,
      requestData.testCode,
      requestData.grade,
    ];

    database.query(query, queryParam, (err: any, result: QueryResult) => {
      if (err) {
        if (err.code == "23505") {
          return res.json({
            status: false,
            message: "Đã tồn tại",
          });
        }
        return res.json({
          status: false,
          message: "Đã có lỗi xảy ra",
        });
      }
      return res.json({
        status: true,
        message: "Thêm thành công",
      });
    });
  } catch (error) {
    return res.json({
      status: false,
      message: "Đã có lỗi xảy ra",
    });
  }
});
/// update subject
app.route("/stu-test/update").post(async (req, res) => {
  try {
    const requestData = req.body;
    if (!requestData.studentCode || !requestData.testCode) {
      return res.json({
        status: false,
        message: "Xin vui lòng điền đầy đủ thông tin",
      });
    }
    var queryParam = [requestData.studentCode, requestData.testCode];
    var query = `
    SELECT *
    FROM student_test
    WHERE
    (
      student_id =
      (
        SELECT student.id
        FROM student
        WHERE student.code = $1
      )
      AND
      test_id =
      (
        SELECT test.id
        FROM test
        WHERE test.code = $2
      )
    )
    `;
    var result: QueryResult = await database.query(query, queryParam);
    if (result.rowCount == 0) {
      return res.json({
        status: true,
        message: "Mã không tồn tại.",
      });
    }
    queryParam.push(requestData.grade);
    var query = `
        UPDATE student_test
        SET  test_id =
        (
          SELECT test.id
          FROM test
          WHERE test.code = $2
        ),
        student_id = 
        (
          SELECT student.id
          FROM student
          WHERE student.code = $1
        ),
        grade = $3
        WHERE 
        (
          student_id =
          (
            SELECT student.id
            FROM student
            WHERE student.code = $1
          ) 

          AND 

          test_id =
          (
            SELECT test.id
            FROM test
            WHERE test.code = $2
          )
        )
        `;

    database.query(query, queryParam, (err: Error, result: QueryResult) => {
      if (err) {
        return res.json({
          status: false,
          message: "Đã có lỗi xảy ra",
        });
      }
      return res.json({
        status: true,
        message: "Cập nhật thành công",
      });
    });
  } catch (error) {
    return res.json({
      status: false,
      message: "Đã có lỗi xảy ra",
    });
  }
});
/// delete subject
app.route("/stu-test/delete").post(async (req, res) => {
  try {
    const requestData = req.body;
    if (!requestData.studentCode || !requestData.testCode) {
      return res.json({
        status: false,
        message: "Xin vui lòng điền đầy đủ thông tin",
      });
    }
    var queryParam = [requestData.studentCode, requestData.testCode];

    var result = await database.query(
      `
    SELECT * 
    FROM student_test 
    WHERE
    (
      student_id =
      (
        SELECT student.id
        FROM student
        WHERE student.code = $1
      )
      AND
      test_id =
      (
        SELECT test.id
        FROM test
        WHERE test.code = $2
      )
    )
    `,
      queryParam
    );

    if (result.rowCount == 0) {
      return res.json({
        status: false,
        message: "Không tồn tại học sinh với bài kiểm tra này.",
      });
    }

    var query = `
        DELETE FROM public.student_test
        WHERE
        (
          student_id =
          (
            SELECT student.id
            FROM student
            WHERE student.code = $1
          )
          AND
          test_id =
          (
            SELECT test.id
            FROM test
            WHERE test.code = $2
          )
        )
        `;

    database.query(query, queryParam, (err: Error, result: QueryResult) => {
      if (err) {
        return res.json({
          status: false,
          message: "Đã có lỗi xảy ra",
        });
      }
      return res.json({
        status: true,
        message: "Xóa thành công",
      });
    });
  } catch (error) {
    return res.json({
      status: false,
      message: "Đã có lỗi xảy ra",
    });
  }
});
